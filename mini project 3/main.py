import pandas as pd
from matplotlib import pyplot as plt
import numpy as np


def calc_least_square(A, y):
    A_prim = A.T @ A
    y_prim = A.T @ y
    x = np.linalg.solve(A_prim, y_prim)
    return x


def calc_error(values_arr, predicted_arr, days, type):
    print("============== {} =================".format(type))
    errors = np.empty(10)
    arr_length = len(values_arr)
    for i in range(arr_length-days, arr_length):
        print("calculated value: {}".format(predicted_arr[i]))
        print("actual value: {}".format(values_arr[i]))
        error = predicted_arr[i] - values_arr[i]
        errors = np.append(errors, error)
        print("error: {}".format(error))
        print("-------------------------------------------------------")
    return errors


def find_better_res_error(linear_error, square_error):
    if len(linear_error) != len(square_error):
        raise Exception("Both Arrays should have equal lengths")
    cmp_res = np.greater(linear_error, square_error)
    if np.count_nonzero(cmp_res) > len(linear_error)/2:
        return "Linear"
    else:
        return "Square"


def plot_show_result(times, Y, regres_arr):
    plt.plot(times, Y, label="GOOGL Stock")
    plt.plot(times, regres_arr, "r", label="Regression")
    plt.legend()
    plt.show()


def calculate_linear_regres(data, ignore_last_index):
    mat = np.array(data.values, "float")
    Y = np.transpose(np.array(mat[:, 0]))
    ones = np.ones(len(Y), dtype=np.int64).T
    times = np.arange(len(Y), dtype=np.int64)
    coefs = np.vstack((ones, times)).T
    # X = np.linalg.inv(coefs).dot(Y)
    upper_bound = len(Y) - ignore_last_index
    b0, b1 = calc_least_square(coefs[:upper_bound], Y[:upper_bound])
    # b0, b1 = np.linalg.lstsq(coefs[:upper_bound], Y[:upper_bound], rcond=None)[0]
    regres_arr = b0 + b1 * times
    # calc_error(Y, regres_arr, 10)
    # plot_show_result(times, Y, regres_arr)
    return times, Y, regres_arr


def calculate_square_regres(data, ignore_last_index):
    mat = np.array(data.values, "float")
    Y = np.transpose(np.array(mat[:, 0]))
    ones = np.ones(len(Y), dtype=np.int64).T
    times = np.arange(len(Y), dtype=np.int64)
    sq_times = np.power(times, 2)
    coefs = np.vstack((ones, times, sq_times)).T
    upper_bound = len(Y) - ignore_last_index
    b0, b1, b2 = calc_least_square(coefs[:upper_bound], Y[:upper_bound])
    # b0, b1, b2 = np.linalg.lstsq(coefs, Y, rcond=None)[0]
    regres_arr = b0 + b1*times + b2*sq_times
    # calc_error(Y, regres_arr, 10)
    # plot_show_result(times, Y, regres_arr)
    return times, Y, regres_arr
    

if __name__ == "__main__":
    df = pd.read_csv("googl.csv")
    data = df[["Close"]]
    last_index_prediction = 10
    li_times, li_y, li_regres_arr = calculate_linear_regres(data, last_index_prediction)
    sq_times, sq_y, sq_regres_arr = calculate_square_regres(data, last_index_prediction)
    li_errors = calc_error(li_y, li_regres_arr, last_index_prediction, "Linear Regression")
    sq_errors = calc_error(sq_y, sq_regres_arr, last_index_prediction, "Square Regression")
    res = find_better_res_error(li_errors, sq_errors)
    if res == "Linear":
        print("Linear Regression has Better Results")
        plot_show_result(li_times, li_y, li_regres_arr)
    elif res == "Square":
        print("Square Regression has Better Results")
        plot_show_result(sq_times, sq_y, sq_regres_arr)