from matplotlib import image, colors
import matplotlib.pyplot as plt
import numpy as np
import matplotlib.transforms as mtransforms

lower_white_arr = np.array([255, 255, 255])
upper_white_arr = np.array([0, 0, 255])
white_arr = np.array([255,255,255])
gray_arr = np.array([65,65,65])
orig_img_rows = 0
orig_img_columns = 0
OFFSET = 50
# cpy_img

#Preparing Original Image and Gray Image
def get_image():
    i = 0
    j = OFFSET
    user_input = input("Enter dir: ")
    print("Processing...")
    # user_input = "r.jpg"
    image_data = image.imread(user_input)
    img_arr = np.array(image_data)
    img_shape = img_arr.shape
    global orig_img_rows
    orig_img_rows = img_shape[0]
    global orig_img_columns
    orig_img_columns = img_shape[1]
    cpy_img = np.full((orig_img_rows, orig_img_columns+OFFSET, 3), 255, dtype=int)
    cpy_img.setflags(write=1)
    while i < orig_img_rows:
        while j < orig_img_columns+OFFSET:
            if not is_in_white_range(img_arr[i][j-OFFSET]):
                cpy_img[i][j] = gray_arr
            j = j + 1
        j = OFFSET
        i = i + 1
    i = j = 0
    return cpy_img, img_arr


def combine_image(orig_img, gray_img):
    i = 0
    j = 0
    final_img = np.full((orig_img_rows, orig_img_columns+OFFSET, 3), 255, dtype=int)
    final_img.setflags(write=1)
    # Combining
    while i < orig_img_rows:
        while j < orig_img_columns:
            if is_in_white_range(orig_img[i][j]):
                if is_in_white_range(gray_img[i][j]):
                    final_img[i][j] = white_arr
                else:
                    final_img[i][j] = gray_arr
            else:
                final_img[i][j] = orig_img[i][j]
            j += 1
        j = 0
        i += 1
    i = 0
    # Completing Shadow with just gray because of ended ORIGINAL IMAGE
    while i < orig_img_rows:
        while j < orig_img_columns+OFFSET:
            if is_in_white_range(gray_img[i][j]):
                final_img[i][j] = white_arr
            else:
                final_img[i][j] = gray_arr
            j += 1
        j = orig_img_columns
        i += 1
    return final_img


def is_in_white_range(img_arr):
    # img_arr = colors.rgb_to_hsv(img_arr)
    # print(img_arr)
    # if (img_arr[2] >= lower_white_arr[2]) and (img_arr[2] <= upper_white_arr[2]):
    #     return True
    # return False
    if (np.greater_equal(img_arr, [230,230,230]).all()):
        return True
    return False


if __name__ == '__main__':
    cpy_img_data, img_data = get_image()
    final_img = combine_image(img_data, cpy_img_data)
    plt.imshow(final_img)

    # plt.imshow(img_data)
    # plt.imshow(cpy_img_data, alpha=1, interpolation='bilinear')
    plt.show()
    # figure.show()
    # figure.savefig("pt.jpg")