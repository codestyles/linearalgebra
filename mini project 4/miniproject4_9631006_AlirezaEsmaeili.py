import numpy as np
import matplotlib.pyplot as plt

k = 1200

def calc_matrix_using_svd(U, S, V):
    img_reduced_noise = np.array(np.dot(U[:, :S.shape[0]] * S, V), dtype=int)
    return img_reduced_noise


def remove_sigma_values(sigma, threshold):
    s_cleaned = np.array([si if si > threshold else 0 for si in sigma])
    print(s_cleaned)
    return s_cleaned


if __name__ == "__main__":
    img = plt.imread("noisy.jpg")

    r_img = img[:,:,0]
    g_img = img[:,:,1]
    b_img = img[:,:,2]

    U_r, S_r, V_r = np.linalg.svd(r_img, full_matrices=True)
    U_g, S_g, V_g = np.linalg.svd(g_img, full_matrices=True)
    U_b, S_b, V_b = np.linalg.svd(b_img, full_matrices=True)

    cleaned_sigma_r = remove_sigma_values(S_r, k)
    cleaned_sigma_g = remove_sigma_values(S_g, k)
    cleaned_sigma_b = remove_sigma_values(S_b, k)

    r_cleaned = calc_matrix_using_svd(U_r, cleaned_sigma_r, V_r)
    g_cleaned = calc_matrix_using_svd(U_g, cleaned_sigma_g, V_g)
    b_cleaned = calc_matrix_using_svd(U_b, cleaned_sigma_b, V_b)

    rgb_uint8 = (np.dstack((r_cleaned, g_cleaned, b_cleaned))).astype(np.uint8)

    # plt.imshow(rgb_uint8)
    plt.imsave("cleaned.jpg", rgb_uint8)
    # plt.show()